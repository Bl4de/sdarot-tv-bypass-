import requests
import time
import re
import json
import sys

WATCH = "https://sdarot.world/ajax/watch"
URL = "https://sdarot.world"
USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
TYPE = "application/x-www-form-urlencoded"
FINAL_URL = "https://{0}/watch/episode/{1}/{2}.mp4?token={3}&time={4}&uid="
REFERER = 'https://sdarot.world/watch/2523-%D7%9E%D7%98%D7%95%D7%9E%D7%98%D7%9E%D7%AA-metumtemet/season/1/episode/1'  # ;)
SERIE_PATTERN = "https:\/\/sdarot\.world\/watch\/([0-9]+)"
USAGE = "Usage: python sdarot_bypass.py <serie url> <season number> <episode number>"
FINAL_MESSAGE = "The url is:\n{0}\nHave fun!"
ERROR_MESSAGE = "Error :("


class InvalidURLException(Exception):
    def __init__(self):
        super(InvalidURLException, self).__init__()

    def __str__(self):
        return "The given URL is invalid"


class WrongNumberOfParameters(Exception):
    def __init__(self):
        super(WrongNumberOfParameters, self).__init__()


class NotANumber(Exception):
    def __init__(self, parameter_name):
        super(NotANumber, self).__init__()
        self.parameter_name = parameter_name

    def __str__(self):
        return "The given {0} should be numeric.".format(self.parameter_name)


class Connection(object):

    def __init__(self):
        self.session = requests.Session()
        self.headers = {
            'referer': REFERER,
            "user-agent": USERAGENT,
            "content-type": TYPE,
            "origin": "https://sdarot.world",
            "X-Requested-With": "XMLHttpRequest",
        }
        self.session.headers.update(self.headers)

    def post(self, url, parameters):
        response = self.session.post(url, parameters)
        response.raise_for_status()
        return response.content

    def get(self, url):
        response = self.session.get(url)
        response.raise_for_status()
        return response.content


def check_parameters():
    """
    Checks the given season, episode, serie and returns the serie
    """
    if len(sys.argv) != 4:
        raise WrongNumberOfParameters()

    serie, season, episode = sys.argv[1:]

    serie = re.findall(SERIE_PATTERN, serie)[0]  # index out of range Exception
    if not season.isdigit():
        raise NotANumber("season")
    elif not episode.isdigit():
        raise NotANumber("episode")
    elif not serie:
        raise InvalidURLException()

    return (serie, season, episode)


def main():
    session = Connection()
    try:

        serie, season, episode = check_parameters()

        response = session.post(WATCH, {"auth": "false", "type": "episode",
                                        "episode": episode, "season": season, "serie": serie, "watch": "true", "token": ""})
        print(response)
        data = json.loads(response)

        for number, token in data["watch"].items():
            pass
        time = data["time"]
        video = data["VID"]
        url = data["url"]

        final_url = FINAL_URL.format(url, number, video, token, time)
        print(FINAL_MESSAGE.format(final_url))

    except WrongNumberOfParameters:
        print(USAGE)
    except Exception as ex:
        print(ex)
        return ERROR_MESSAGE


if __name__ == "__main__":
    main()
